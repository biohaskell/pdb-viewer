---
title: Getting Started
---

rio-template has two commands, `config` and `run`. Here are some example use cases

```bash
app-exe config
```

```bash
app-exe run
```
