---
title: Home
---

# rio-etc-template - A simple application tempalate using RIO and etc.

This is the soft documentation site for the rio-etc-template.

This serves as a place to provide detailed descriptions about how to
use your application or library.

![build](https://gitlab.com/zenhaskell/rio-etc-template/badges/master/build.svg)
