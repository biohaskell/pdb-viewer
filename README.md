# pdb-viewer

Gloss based viewer for the pdb protein databank format.

## Building

Build with

    stack build

Run with

    stack exec -- pdb-viewer
